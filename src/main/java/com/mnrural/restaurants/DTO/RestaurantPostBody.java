package com.mnrural.restaurants.DTO;

/**
 * Created by MStommes on 10/10/18.
 */
public class RestaurantPostBody {

    String name;
    String type;
    String city;

    public RestaurantPostBody() {
    }

    public RestaurantPostBody(String type, String name, String city) {
        this.type = type;
        this.name = name;
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
