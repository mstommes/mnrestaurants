package com.mnrural.restaurants.controllers;

import com.mnrural.restaurants.DTO.RestaurantPostBody;
import com.mnrural.restaurants.entities.RestaurantEntity;
import com.mnrural.restaurants.service.RestaurantService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value={"/restaurant"})
public class RestaurantController {

    RestaurantService restaurantService;

    public RestaurantController(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }


    @GetMapping
    public ResponseEntity<List<RestaurantEntity>>getRestaurantList()
    {
        return new ResponseEntity<List<RestaurantEntity>>(restaurantService.getAllRestaurants(), HttpStatus.OK);
    }

    @GetMapping("/selected")
    public ResponseEntity<List<RestaurantEntity>>  getRestaurantById(@RequestParam("city")String city){
        return new ResponseEntity<List<RestaurantEntity>>(restaurantService.getRestaurantByCity(city), HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<RestaurantEntity> addNewRestaurant(@RequestBody RestaurantPostBody postBody){
        return new ResponseEntity<RestaurantEntity>(restaurantService.addRestaurant(postBody), HttpStatus.OK);
    }

    @PutMapping("/update")
    public ResponseEntity<RestaurantEntity> updateRestaurant(@RequestBody RestaurantPostBody postBody, @RequestParam("id") long id){
        return new ResponseEntity<RestaurantEntity>(restaurantService.updateRestaurant(postBody, id), HttpStatus.OK);
    }
//
//    @PatchMapping("/partial/update")
//    public String partialUpdateRestaurant(@RequestBody String something){
//        return "partial update restaurant";
//    }

    @DeleteMapping()
    public String deleteRestaurant(@RequestParam("id") long id){
        restaurantService.deleteRestaurant(id);
        return "Restaurant Successfully Deleted";
    }

}



