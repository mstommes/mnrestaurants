package com.mnrural.restaurants.config;

import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * Created by MStommes on 10/9/18.
 */
@EnableTransactionManagement
public class ConfigurationBean {
}
