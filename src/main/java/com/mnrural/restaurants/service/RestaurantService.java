package com.mnrural.restaurants.service;

import com.mnrural.restaurants.DTO.RestaurantPostBody;
import com.mnrural.restaurants.Repositories.RestaurantRepository;
import com.mnrural.restaurants.entities.RestaurantEntity;

import org.hibernate.annotations.SourceType;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by MStommes on 10/9/18.
 */
@Service
public class RestaurantService {

    private String username = "my secret username";
    private String password = "my secret password";

    RestaurantRepository restaurantRepository;

    public RestaurantService(RestaurantRepository restaurantRepository) {
        this.restaurantRepository = restaurantRepository;
    }

    public List<RestaurantEntity> getAllRestaurants(){
        //very obvious secret injection
        System.out.println(username + password);
        return restaurantRepository.findAll();
    }

    public List<RestaurantEntity>  getRestaurantByCity(String city){
        return restaurantRepository.findByCity(city);
    }

    public RestaurantEntity addRestaurant(RestaurantPostBody postBody){
        RestaurantEntity entity = new RestaurantEntity();
        entity.setName(postBody.getName());
        entity.setCity(postBody.getCity());
        entity.setType(postBody.getType());
        return restaurantRepository.save(entity);
    }

    public RestaurantEntity updateRestaurant(RestaurantPostBody postBody, long id){
        RestaurantEntity entity = restaurantRepository.findById(id);
        entity.setName(postBody.getName());
        entity.setCity(postBody.getCity());
        entity.setType(postBody.getType());
        return restaurantRepository.save(entity);
    }

    public RestaurantEntity deleteRestaurant(long id){
        return restaurantRepository.deleteById(id);
    }



}
