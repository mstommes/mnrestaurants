package com.mnrural.restaurants.Repositories;

import com.mnrural.restaurants.entities.RestaurantEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by MStommes on 10/9/18.
 */
@Repository
public interface RestaurantRepository extends JpaRepository<RestaurantEntity, Long>{
    List<RestaurantEntity> findAll();
    List<RestaurantEntity> findByCity(String city);
    RestaurantEntity save(RestaurantEntity entity);
    RestaurantEntity findById(long id);
    RestaurantEntity deleteById(long id);
}
